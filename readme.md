# Jenkins
> This repo houses example files for setting up jenkins within docker containers

## Setup
---
- Create Jenkins Home
- Start docker containers
- Connect to UI
- Configure jenkins agents


### Create jenkins_home
---
> Use this directory to store ssh keys created for jenkins agent, ex : /jenkins_home/.ssh/
- Jenkins home directory must be created prior to starting up jenkins container
```
mkdir jenkins_home
```

### Starting Docker Containers
---
- Adjust ports within docker-compose file if needed
- Run docker-compose to start containers
  ```
  docker-compose up -d
  ```
- Connect to UI
  - http://`<hostname>`:8080
  - Follow steps outlined on jenkins ui for setup


### Jenkins agents
---
> Jenkins agents are remote hosts utilized to run jobs or tasks
- Follow readme inside of jenkins_agents directory for example of setting up agent

