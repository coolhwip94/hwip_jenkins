# Jenkins Agent
> A jenkins agent is essentially a remote host that we can define for jenkins to use to run jobs or tasks

### Create an ssh key for jenkins agent
---
> Create an ssh key that our jenkins instance can use to remotely authenticate with the jenkins agent
```
ssh-keygen -f /home/ltruong/hwip_jenkins/jenkins_home/.ssh
```

### Configure Jenkins Agent
---
- Within the UI add a new credential
- Use the ssh private key generated above for the credential

### Assign SSH key variable
---
> The docker-compose file will use this variable inside the container to authenticate the jenkins user.

```
export JENKINS_AGENT_SSH_PUBKEY=$(cat /home/ltruong/hwip_jenkins/jenkins_home/.ssh/jenkins_agent_key.pub)
```

### Running docker containers
---
```
docker-compose up -d
```

### Create new node in Jenkins UI
---
> Register remote host as jenkins agent
1. Jenkins Dashboard > Manage Jenkins > Manage Nodes and Clouds
2. Create 'New Node'
   - Name : agent1
   - Type: permanent
3. Fill in the fields
   - remote root : `/home/jenkins/`
   - labels : `agent1`
   - usage : `only build jobs with label expressions matching this node`
   - launch method : `launch agents via ssh`
   - credentials : `jenkins` - use the one created in previous steps
   - host key verification : `manually trusted key verification strategy`
  
### Create Test Project to run on agent
---
1. Dashboard > New Item > Freestyle Project
   - name : `job for agent1`
2. Configure Project
   - select `restrict where this project can be run`
     - label expression: `agent1`
3. Add a simple build step
   - In `build` section select `execute shell`
   - shell command can be something simple like
    ```
    echo $NODE_LABELS
    ```
4. Save
5. Click `build now`



### References
---
- https://www.jenkins.io/doc/book/using/using-agents/
